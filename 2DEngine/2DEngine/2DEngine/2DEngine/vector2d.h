#ifndef __VECTOR2D_H__
#define __VECTOR2D_H__

class vector2d
{
	public:
		float x, y , w;

		vector2d(float x, float y);
		vector2d(vector2d& v);

		vector2d operator+(const vector2d& rhs);
		vector2d operator-(const vector2d& rhs);

		vector2d& operator+=(const vector2d& rhs);
		vector2d& operator-=(const vector2d& rhs);

		float operator*(const vector2d& rhs);

		float crossProduct(const vector2d& vec);

		vector2d normalize();

		float magnitude();
};

#endif
