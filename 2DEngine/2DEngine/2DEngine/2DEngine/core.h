#ifndef __CORE_H__
#define __CORE_H__

#include "Scene.h"

namespace rmsoft
{
	class RMRenderer
	{
		private :
			Scene *currentScene;
		public:
			RMRenderer();
			
			void run(float deltaTime);
			void setScene(Scene * scene);
			Scene *getScene();

			~RMRenderer();
	};
}

#endif
