#ifndef __MATRIX3X3_H__
#define __MATRIX3X3_H__

#include <iostream>

using std::ostream;


class vector2d;

const float PI = 3.141316;

class Matrix3x3
{
	public:
		float matrix[3][3];

		Matrix3x3();

		void setIdentity();
		void translate(float tx, float ty);
		void rotate(float angle);
		void scale(float sx, float sy);
	
		void transform(vector2d&);

		void Matrix3x3::mutliply(const float(&matrix)[3][3]);
		float* Matrix3x3::getPtr();

		Matrix3x3 &orthoMatrix(float left, float right, float bottom, float top);
		Matrix3x3 &Matrix3x3::mutliply(const float(&matrixa)[3][3], const float(&matrixb)[3][3]);

		friend ostream& operator<<(ostream& os, Matrix3x3&);
};

#endif
