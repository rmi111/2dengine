#include <stdio.h>  
#include <stdlib.h>  
#include <gl\glew.h>
#include <GL/freeglut.h>
#include <GLFW\glfw3.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include "Quad.h"
#include "SpriteBatcher.h"
#include "vector2d.h"

void renderScene();

static void error_callback(int error, const char* description)
{
	fputs(description, stderr);
	_fgetchar();
}

void showFPS(GLFWwindow *pWindow);
void key_callback(GLFWwindow* window, int key, int scancode, int action, int mode);

Quad *quad;
float lastTime = 0.0f;
int nbFrames = 0;

int main(int argc, char **argv)
{
	const int SCREEN_H = 600;
	const int SCREEN_W = 800;

	glfwInit();
	// Set all the required options for GLFW
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
	glfwWindowHint(GLFW_RESIZABLE, GL_FALSE);

	float aspectRatio = ((float)SCREEN_W / (float)SCREEN_H);// : ((float)SCREEN_H / (float)SCREEN_W);

	// Create a GLFWwindow object that we can use for GLFW's functions
	GLFWwindow* window = glfwCreateWindow(SCREEN_W, SCREEN_H, "LearnOpenGL", nullptr, nullptr);
	
	glfwMakeContextCurrent(window);

	// Set the required callback functions
	glfwSetKeyCallback(window, key_callback);

	// Set this to true so GLEW knows to use a modern approach to retrieving function pointers and extensions
	glewExperimental = GL_TRUE;
	// Initialize GLEW to setup the OpenGL Function pointers
	glewInit();

	// Define the viewport dimensions
	glViewport(0, 0, 800, 600);
	
	//SpriteBatcher batcher;

	//quad = new Quad(1, 1, "vertex_shader.glsl", "fragment_shader.glsl");

	Quad *quads[10000];

	int posx = 20;
	int posy = 100;
	
	for (int i = 0; i < 10000; ++i)
	{
		quads[i] = new Quad(vector2d(posx,posy),20, 20, "vertex_shader.glsl", "fragment_shader.glsl");

		//batcher.addSprite(*quads[i]);

		posx += 50;		

		if (posx > 800)
		{
			posx = 20;
			posy += 50;
		}
	}

	//batcher.flushBatch();

	// Game loop
	while (!glfwWindowShouldClose(window))
	{
		// Check if any events have been activiated (key pressed, mouse moved etc.) and call corresponding response functions
		glfwPollEvents();

		// Render
		// Clear the colorbuffer
		glClearColor(0.2f, 0.3f, 0.3f, 1.0f);
		glClear(GL_COLOR_BUFFER_BIT);

		//batcher.draw();
		for (int i = 0; i < 100; ++i)
		{
			quads[i]->draw(aspectRatio);
		}

		//quad->draw(aspectRatio);

		glfwSwapBuffers(window);

		showFPS(window);
	}


	// Terminate GLFW, clearing any resources allocated by GLFW.
	glfwTerminate();
	

	for (int i = 0; i < 10000; ++i)
	{
		delete quads[i];
	}
}

// Is called whenever a key is pressed/released via GLFW
void key_callback(GLFWwindow* window, int key, int scancode, int action, int mode)
{
	if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS)
		glfwSetWindowShouldClose(window, GL_TRUE);
	/*else if (key == GLFW_KEY_RIGHT)
		//quad->increaseAngle();
	else if (key == GLFW_KEY_LEFT)
		//quad->decreaseAngle();
	//quad->rotate(angle);*/
}

void showFPS(GLFWwindow *pWindow)
{
	// Measure speed
	double currentTime = glfwGetTime();
	double delta = currentTime - lastTime;
	nbFrames++;
	if (delta >= 1.0) { // If last cout was more than 1 sec ago
		std::cout << 1000.0 / double(nbFrames) << std::endl;

		double fps = double(nbFrames) / delta;

		std::stringstream ss;
		ss << " [" << fps << " FPS]";

		glfwSetWindowTitle(pWindow, ss.str().c_str());

		nbFrames = 0;
		lastTime = currentTime;
	}
}

void renderScene()
{

/*	quad->rotate(10);
	quad->draw();*/

	//delete quad;
}