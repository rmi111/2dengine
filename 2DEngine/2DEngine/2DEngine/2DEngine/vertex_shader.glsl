	#version 330 core
  
	layout (location = 0) in vec2 a_position;

	uniform mat3 u_matrix;
	//uniform mat3 projection;

	void main()
	{
		//gl_Position = vec4( transform * vec3(position.x,position.y,1.0) , 1.0);

		//gl_Position = vec4(u_matrix * vec3(a_position.x, a_position.y, 1.0), 1.0);
		//gl_Position = vec4((transform * vec3(position, 1)).xy, 0, 1);
		gl_Position = vec4((u_matrix * vec3(a_position, 1)).xy, 1.0, 1);
	}

