#ifndef __SCENE_H__
#define __SCENE_H__

namespace rmsoft
{
	class Scene
	{
		public:
			Scene();
			virtual void init();
			virtual void update(float deltaTime) = 0;
			virtual void drawScene(float deltaTime) = 0;
			~Scene();
	};
}

#endif