#include "matrix3x3.h"
#include "vector2d.h"
#include <cmath>

Matrix3x3::Matrix3x3()
{
	matrix[0][0] = 1.0f;
	matrix[0][1] = 0.0f;
	matrix[0][2] = 0.0f;

	matrix[1][0] = 0.0f;
	matrix[1][1] = 1.0f;
	matrix[1][2] = 0.0f;

	matrix[2][0] = 0.0f;
	matrix[2][1] = 0.0f;
	matrix[2][2] = 1.0f;
}

void Matrix3x3::setIdentity()
{
	matrix[0][0] = 1.0f;
	matrix[0][1] = 0.0f;
	matrix[0][2] = 0.0f;

	matrix[1][0] = 0.0f;
	matrix[1][1] = 1.0f;
	matrix[1][2] = 0.0f;

	matrix[2][0] = 0.0f;
	matrix[2][1] = 0.0f;
	matrix[2][2] = 1.0f;
}

void Matrix3x3::translate(float tx, float ty)
{
	float tmpMatrix[3][3];

	tmpMatrix[0][0] = 1.0f;
	tmpMatrix[0][1] = 0.0f;
	tmpMatrix[0][2] = 0.0f;

	tmpMatrix[1][0] = 0.0f;
	tmpMatrix[1][1] = 1.0f;
	tmpMatrix[1][2] = 0.0f;

	tmpMatrix[2][0] = tx;
	tmpMatrix[2][1] = ty;
	tmpMatrix[2][2] = 1.0f;

	mutliply(tmpMatrix);
}

void Matrix3x3::rotate(float angle)
{
	float tmpMatrix[3][3];

	const float radian = (3.141592653589793f) / 180.0f; //(angle * 3.141592653589793 )/ 180.0;// (2 * PI) / (360.0 / cos(angle));

	float cs = cos(radian * angle);
	float sn = sin(radian * angle);

	tmpMatrix[0][0] = cs;
	tmpMatrix[0][1] = -sn;
	tmpMatrix[0][2] = 0.0f;

	tmpMatrix[1][0] = sn;
	tmpMatrix[1][1] = cs;
	tmpMatrix[1][2] = 0.0f;

	tmpMatrix[2][0] = 0.0f;
	tmpMatrix[2][1] = 0.0f;
	tmpMatrix[2][2] = 1.0f;

	mutliply(tmpMatrix);
}

void Matrix3x3::scale(float sx, float sy)
{
	float tmpMatrix[3][3];

	tmpMatrix[0][0] = sx;
	tmpMatrix[0][1] = 0.0f;
	tmpMatrix[0][2] = 0.0f;

	tmpMatrix[1][0] = 0.0f;
	tmpMatrix[1][1] = sy;
	tmpMatrix[1][2] = 0.0f;

	tmpMatrix[2][0] = 0.0f;
	tmpMatrix[2][1] = 0.0f;
	tmpMatrix[2][2] = 1.0f;

	mutliply(tmpMatrix);
}

void Matrix3x3::transform(vector2d& vec)
{
	vec.w = 1;

	float tx = vec.x * matrix[0][0] + vec.y * matrix[1][0] + vec.w * matrix[2][0];
	float ty = vec.x * matrix[0][1] + vec.y * matrix[1][1] + vec.w * matrix[2][1];
	float tw = vec.x * matrix[0][2] + vec.y * matrix[1][2] + vec.w * matrix[2][2];

	vec.x = tx;
	vec.y = ty;
	vec.w = tw;
}

void Matrix3x3::mutliply(const float(&matrixb)[3][3])
{
	setIdentity();

	float tmat[3][3];

	tmat[0][0] = (matrix[0][0] * matrixb[0][0]) + (matrix[0][1] * matrixb[1][0]) + (matrix[0][2] * matrixb[2][0]);
	tmat[0][1] = (matrix[0][0] * matrixb[0][1]) + (matrix[0][1] * matrixb[1][1]) + (matrix[0][2] * matrixb[2][1]);
	tmat[0][2] = (matrix[0][0] * matrixb[0][2]) + (matrix[0][1] * matrixb[1][2]) + (matrix[0][2] * matrixb[2][2]);

	tmat[1][0] = (matrix[1][0] * matrixb[0][0]) + (matrix[1][1] * matrixb[1][0]) + (matrix[1][2] * matrixb[2][0]);
	tmat[1][1] = (matrix[1][0] * matrixb[0][1]) + (matrix[1][1] * matrixb[1][1]) + (matrix[1][2] * matrixb[2][1]);
	tmat[1][2] = (matrix[1][0] * matrixb[0][2]) + (matrix[1][1] * matrixb[1][2]) + (matrix[1][2] * matrixb[2][2]);

	tmat[2][0] = (matrix[2][0] * matrixb[0][0]) + (matrix[2][1] * matrixb[1][0]) + (matrix[2][2] * matrixb[2][0]);
	tmat[2][1] = (matrix[2][0] * matrixb[0][1]) + (matrix[2][1] * matrixb[1][1]) + (matrix[2][2] * matrixb[2][1]);
	tmat[2][2] = (matrix[2][0] * matrixb[0][2]) + (matrix[2][1] * matrixb[1][2]) + (matrix[2][2] * matrixb[2][2]);

	for (int i = 0; i < 3; ++i)
	{
		for (int j = 0; j < 3; ++j)
			matrix[i][j] = tmat[i][j];
	}
}

Matrix3x3 &Matrix3x3::mutliply(const float(&matrixa)[3][3], const float(&matrixb)[3][3])
{
	Matrix3x3 mat;

	float tmat[3][3];

	tmat[0][0] = (matrixa[0][0] * matrixb[0][0]) + (matrixa[0][1] * matrixb[1][0]) + (matrixa[0][2] * matrixb[2][0]);
	tmat[0][1] = (matrixa[0][0] * matrixb[0][1]) + (matrixa[0][1] * matrixb[1][1]) + (matrixa[0][2] * matrixb[2][1]);
	tmat[0][2] = (matrixa[0][0] * matrixb[0][2]) + (matrixa[0][1] * matrixb[1][2]) + (matrixa[0][2] * matrixb[2][2]);

	tmat[1][0] = (matrixa[1][0] * matrixb[0][0]) + (matrixa[1][1] * matrixb[1][0]) + (matrixa[1][2] * matrixb[2][0]);
	tmat[1][1] = (matrixa[1][0] * matrixb[0][1]) + (matrixa[1][1] * matrixb[1][1]) + (matrixa[1][2] * matrixb[2][1]);
	tmat[1][2] = (matrixa[1][0] * matrixb[0][2]) + (matrixa[1][1] * matrixb[1][2]) + (matrixa[1][2] * matrixb[2][2]);

	tmat[2][0] = (matrixa[2][0] * matrixb[0][0]) + (matrixa[2][1] * matrixb[1][0]) + (matrixa[2][2] * matrixb[2][0]);
	tmat[2][1] = (matrixa[2][0] * matrixb[0][1]) + (matrixa[2][1] * matrixb[1][1]) + (matrixa[2][2] * matrixb[2][1]);
	tmat[2][2] = (matrixa[2][0] * matrixb[0][2]) + (matrixa[2][1] * matrixb[1][2]) + (matrix[2][2] * matrixb[2][2]);

	for (int i = 0; i < 3; ++i)
		for (int j = 0; j < 3; ++j)
			mat.matrix[i][j] = tmat[i][j];

	return mat;
}

Matrix3x3 &Matrix3x3::orthoMatrix(float left, float right, float bottom, float top)
{
	Matrix3x3 mat;

	float tmat[3][3];

	tmat[0][0] = 2.0f / (right - left);
	tmat[0][1] = 0.0f;
	tmat[0][2] = 0.0f;

	tmat[1][0] = 0.0f;
	tmat[1][1] = 2.0f / (top - bottom);
	tmat[1][2] = 0.0f;

	tmat[2][0] = -((right + left) / (right - left));
	tmat[2][1] = -((top + bottom) / (top - bottom));
	tmat[2][2] = 1.0f;

	for (int i = 0; i < 3; ++i)
		for (int j = 0; j < 3; ++j)
			mat.matrix[i][j] = tmat[i][j];

	return mat;
}

std::ostream& operator<<(std::ostream& os, Matrix3x3& matrix)
{
	os << matrix.matrix[0][0] << " " << matrix.matrix[0][1] << " " << matrix.matrix[0][2] << std::endl;
	os << matrix.matrix[1][0] << " " << matrix.matrix[1][1] << " " << matrix.matrix[1][2] << std::endl;
	os << matrix.matrix[2][0] << " " << matrix.matrix[2][1] << " " << matrix.matrix[2][2] << std::endl;

	return os;
}


float* Matrix3x3::getPtr()
{
	return &matrix[0][0];
}