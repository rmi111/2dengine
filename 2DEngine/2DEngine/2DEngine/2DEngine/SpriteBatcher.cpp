#include "Quad.h"
#include "SpriteBatcher.h"

const size_t VERTEX_COUNT		   = 12;
const size_t MAX_BATCH_COUNT	   = 100;
const size_t INDEX_SIZE_PER_SPRITE = 6;

SpriteBatcher::SpriteBatcher() : spriteCount(0),vertexOffset(0), indexOffset(0), index(0)
{
	vBuffer = new float[(VERTEX_COUNT * MAX_BATCH_COUNT)]();
	iBuffer = new float[(INDEX_SIZE_PER_SPRITE * MAX_BATCH_COUNT)]();	
	
	const char* str  = "vertex_shader.glsl";
	const char* str2 = "fragment_shader.glsl";

	this->shader = new Shader("vertex_shader.glsl", "fragment_shader.glsl");
}

void SpriteBatcher::addSprite(const Quad &quad)
{
	index = spriteCount * 4;
	
	vBuffer[vertexOffset++] = quad.vertexData[0];
	vBuffer[vertexOffset++] = quad.vertexData[1];
	vBuffer[vertexOffset++] = quad.vertexData[2];
	vBuffer[vertexOffset++] = quad.vertexData[3];
	vBuffer[vertexOffset++] = quad.vertexData[4];
	vBuffer[vertexOffset++] = quad.vertexData[5];
	vBuffer[vertexOffset++] = quad.vertexData[6];
	vBuffer[vertexOffset++] = quad.vertexData[7];
	vBuffer[vertexOffset++] = quad.vertexData[8];
	vBuffer[vertexOffset++] = quad.vertexData[9];
	vBuffer[vertexOffset++] = quad.vertexData[10];
	vBuffer[vertexOffset++] = quad.vertexData[11];

	iBuffer[indexOffset++] = index + 0;
	iBuffer[indexOffset++] = index + 1;
	iBuffer[indexOffset++] = index + 2;

	iBuffer[indexOffset++] = index + 0;
	iBuffer[indexOffset++] = index + 2;
	iBuffer[indexOffset++] = index + 3;

	std::cout << "index: " << indexOffset << std::endl;

	++spriteCount;	
}

void SpriteBatcher::flushBatch()
{
	vbaArray = new VertexBufferArray(vBuffer, (VERTEX_COUNT * MAX_BATCH_COUNT), iBuffer, (INDEX_SIZE_PER_SPRITE * MAX_BATCH_COUNT));
	
	vbaArray->setVertexAttribute(0, 0, 3);
	
	Matrix3x3 proj;
	
	proj = proj.orthoMatrix(0, 800.0f, 0, 640.0f);
		
	glUseProgram(shader->getProgramId());

	shader->setUniformMatrix3("u_matrix", proj.getPtr());
}

void SpriteBatcher::draw()
{
	glUseProgram(shader->getProgramId());
	glBindVertexArray(vbaArray->getVaoId());
	
	int size = ((VERTEX_COUNT * MAX_BATCH_COUNT) + (INDEX_SIZE_PER_SPRITE * MAX_BATCH_COUNT)) / 3;

	glDrawElements(GL_TRIANGLES, size , GL_UNSIGNED_INT, 0);
	glBindVertexArray(0);
}

SpriteBatcher::~SpriteBatcher()
{
	delete vBuffer;
	delete iBuffer;
}