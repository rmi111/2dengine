#ifndef __QUAD_H__
#define __QUAD_H__

#include "Shader.h"
#include "matrix3x3.h"
#include "VertexBufferArray.h"

class Quad
{
	friend class SpriteBatcher;

	private:
		size_t width;
		size_t height;
		Shader *shader;				
		VertexBufferArray *vbaArray;
		Matrix3x3 *transformMatrix;
		float indicesData[6];
		float vertexData[12];
		float angle;
		float trans;
	public:
		Quad(size_t width, size_t height, std::string vertexShaderFileName, std::string fragmentShaderFileName);
		Quad(vector2d pos,size_t width, size_t height, std::string vertexShaderFileName, std::string fragmentShaderFileName);
		void draw(float);
		void Quad::rotate(float angle,float);
		~Quad();
		void Quad::increaseAngle();
		void Quad::decreaseAngle();

		void Quad::initQuad(vector2d pos);
	private :
		void initQuad();
};

#endif