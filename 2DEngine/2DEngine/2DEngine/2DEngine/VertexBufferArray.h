#ifndef __VERTEX_BUFFER_ARRAY_H__
#define __VERTEX_BUFFER_ARRAY_H__

#include <GL/glew.h>

class VertexBufferArray
{
	private:		
		GLuint VBO, VAO, EBO;
		bool hasIndices;
		float *vertexData;
		int *indicesData;	
		size_t indexCount;
		size_t vertexCount;		
	public:
		VertexBufferArray(float *vertexData,int vertexDataSize);
		VertexBufferArray(float *vertexData,int vertexDataSize, float *indicesData,int indexDataSize);
		void setVertexAttribute(int attributePosition,int startPosition,int componentCount);
		GLuint getVaoId();
		GLuint getEbaId();
};

#endif