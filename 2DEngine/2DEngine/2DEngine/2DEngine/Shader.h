#ifndef __SHADER_H__
#define __SHADER_H__

#include <string>
#include <fstream>
#include <sstream>
#include <iostream>
#include <GL/glew.h>

class Shader
{
	private:
		GLuint programId;
	public:
		Shader(const GLchar* vertextShaderPath, const GLchar* fragmentShaderPath);
		GLuint getProgramId();
		void useProgram();
		void Shader::setUniformMatrix3(const GLchar *uniformName, float *value);
		void Shader::setUniformMatrix4(const GLchar *uniformName, float *value);
};

#endif