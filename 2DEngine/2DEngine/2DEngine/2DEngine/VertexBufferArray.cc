#include <cstdio>
#include <iostream>
#include "VertexBufferArray.h"

VertexBufferArray::VertexBufferArray(float *vertexsData, int vertexDataSize): indexCount(0), vertexCount(0) /*:
									 indicesData(NULL),hasIndices(false)*/
{
	vertexData[0] = 0.5f;
	vertexData[1] = 0.5f;
	vertexData[2] = 0;

	vertexData[3] = 0.5f;
	vertexData[4] = -0.5f;
	vertexData[5] = 0;

	vertexData[6] = -0.5f;
	vertexData[7] = -0.5f;
	vertexData[8] = 0;

	vertexData[9] = -0.5f;
	vertexData[10] = 0.5f;
	vertexData[11] = 0;

	indicesData[0] = 0;
	indicesData[1] = 1;
	indicesData[2] = 3;

	indicesData[3] = 1;
	indicesData[4] = 2;
	indicesData[5] = 3;

	glGenVertexArrays(1, &VAO);
	glGenBuffers(1,&VBO);
	glGenBuffers(1, &EBO);
}

VertexBufferArray::VertexBufferArray(float *vertexsData, int vertexDataSize, float *indicesData, int indexDataSize) :
				                     hasIndices(true), indexCount(0), vertexCount(0)
{
	this->vertexCount = vertexDataSize;
	this->indexCount  = indexDataSize;

	this->vertexData = new float[vertexDataSize];

	for (int i = 0; i < vertexDataSize; ++i)
		this->vertexData[i] = vertexsData[i];

	this->indicesData = new int[indexDataSize];

	for (int i = 0; i < indexDataSize; ++i)
		this->indicesData[i] = indicesData[i];

	glGenVertexArrays(1, &VAO);
	glGenBuffers(1, &VBO);
	glGenBuffers(1, &EBO);
}

void VertexBufferArray::setVertexAttribute(int attributePosition, int startPosition, int componentCount)
{
	glBindVertexArray(VAO);

	std::cout << vertexData;

	glBindBuffer(GL_ARRAY_BUFFER, VBO);
	glBufferData(GL_ARRAY_BUFFER, sizeof(vertexData) * vertexCount, vertexData, GL_STATIC_DRAW);

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, EBO);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(int) * indexCount, indicesData, GL_STATIC_DRAW);
	
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, componentCount * sizeof(GLfloat),(GLvoid *) 0);
	glEnableVertexAttribArray(0);

	glBindBuffer(GL_ARRAY_BUFFER, 0); 
	
	glBindVertexArray(0);
}

GLuint VertexBufferArray::getVaoId()
{
	return this->VAO;
}

GLuint VertexBufferArray::getEbaId()
{
	return this->EBO;
}
