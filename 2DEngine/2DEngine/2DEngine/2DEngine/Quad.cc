#include "Quad.h"
#include "vector2d.h"
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

Quad::Quad(size_t width, size_t height, std::string vertexShaderFileName, std::string fragmentShaderFileName)
{
	this->width  = width;
	this->height = height;

	this->shader = new Shader(vertexShaderFileName.c_str(), fragmentShaderFileName.c_str());
	
	this->transformMatrix = new Matrix3x3();

	angle = 0.0f;
	trans = 0.0f;

	initQuad();
}

Quad::Quad(vector2d pos, size_t width, size_t height, std::string vertexShaderFileName, std::string fragmentShaderFileName)
{
	this->width  = width;
	this->height = height;

	this->shader = new Shader(vertexShaderFileName.c_str(), fragmentShaderFileName.c_str());

	this->transformMatrix = new Matrix3x3();

	angle = 0.0f;
	trans = 0.0f;

	initQuad(pos);
}

void Quad::initQuad()
{
	float left   = 0 - (width / 2);
	float right  = 0 + (width / 2);
	float top    = 0 - (height / 2);
	float bottom = 0 + (height / 2);

	vertexData[0] =  -50 ;
	vertexData[1] =  -50 ;
	vertexData[2] =    0 ;

	vertexData[3] =  50 ;
	vertexData[4] = -50 ;
	vertexData[5] =   0 ;

	vertexData[6] = 50 ;
	vertexData[7] = 50 ;
	vertexData[8] =  0 ;

	vertexData[9]  = -50 ;
	vertexData[10] =  50 ;
	vertexData[11] =   0 ;
	
	indicesData[0] = 0;
	indicesData[1] = 1;
	indicesData[2] = 2;

	indicesData[3] = 0;
	indicesData[4] = 2;
	indicesData[5] = 3;

	this->vbaArray = new VertexBufferArray(vertexData,12,indicesData,6);

	this->vbaArray->setVertexAttribute(0, 0, 3);
}

void Quad::initQuad(vector2d pos)
{
	float left   = pos.x - (width / 2);
	float right  = pos.x + (width / 2);
	float top    = pos.y + (height / 2);
	float bottom = pos.y - (height / 2);

	vertexData[0] = left;
	vertexData[1] = bottom;
	vertexData[2] = 0;

	vertexData[3] = right;
	vertexData[4] = bottom;
	vertexData[5] = 0;

	vertexData[6] = right;
	vertexData[7] = top;
	vertexData[8] = 0;

	vertexData[9]  = left;
	vertexData[10] = top;
	vertexData[11] = 0;

	indicesData[0] = 0;
	indicesData[1] = 1;
	indicesData[2] = 2;

	indicesData[3] = 0;
	indicesData[4] = 2;
	indicesData[5] = 3;

	this->vbaArray = new VertexBufferArray(vertexData, 12, indicesData, 6);

	this->vbaArray->setVertexAttribute(0, 0, 3);
}

void Quad::rotate(float angle2,float aspect)
{
	/*Matrix3x3 rot;
	Matrix3x3 trans;
	Matrix3x3 proj;
	Matrix3x3 scale;

	proj = proj.orthoMatrix(0 ,800, 0 , 640 );*/
	Matrix3x3 mat;
	Matrix3x3 proj;
	Matrix3x3 scale;
	Matrix3x3 rotate;
	Matrix3x3 trans;

	proj = proj.orthoMatrix(0, 800.0f, 0, 640.0f);
	
	trans.translate(200.0f, 200.0f);
	scale.scale(1.6f, 1.0f);
	rotate.rotate(angle);

	mat = mat.mutliply(scale.matrix, rotate.matrix);
	//mat = mat.mutliply(mat.matrix, rotate.matrix);
	mat = mat.mutliply(mat.matrix, trans.matrix);
	mat = mat.mutliply(mat.matrix, proj.matrix);

	glUseProgram(shader->getProgramId());	

    /*rot.rotate(angle);	
	trans.translate(this->trans,0.0f);
	scale.scale(5.6f, 1.0f);

	Matrix3x3 pr = pr.mutliply( trans.matrix, scale.matrix );
	
	//pr = pr.mutliply(pr.matrix, trans.matrix);
	pr = pr.mutliply(proj.matrix, pr.matrix );*/
	
	shader->setUniformMatrix3("u_matrix", mat.getPtr());
	//shader->setUniformMatrix3("projection", proj.getPtr());

	this->trans += .1f;
}

void Quad::increaseAngle()
{
	angle += 1.0f;
}

void Quad::decreaseAngle()
{
	angle -= 1.0f;
}

void Quad::draw(float aspect)
{
	glUseProgram(shader->getProgramId());	
	glBindVertexArray(vbaArray->getVaoId());
	rotate(0, aspect);
	glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, 0);
	glBindVertexArray(0);
}

Quad::~Quad()
{
	delete shader;
	delete vbaArray;
}