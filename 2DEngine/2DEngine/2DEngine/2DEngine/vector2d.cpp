#include "vector2d.h"
#include <cmath>

vector2d::vector2d(float x,float y) :x(x),y(y),w(1)
{

}

vector2d::vector2d(vector2d& vector) : x(vector.x), y(vector.y), w(1)
{

}

vector2d vector2d::operator+(const vector2d& vector)
{
	return vector2d(this->x + vector.x, this->y + vector.y);
}

vector2d vector2d::operator-(const vector2d& vector)
{
	return vector2d(this->x - vector.x, this->y - vector.y);
}

vector2d& vector2d::operator+=(const vector2d& vector)
{
	this->x += vector.x;
	this->y += vector.y;

	return *this;
}

vector2d& vector2d::operator-=(const vector2d& vector)
{
	this->x -= vector.x;
	this->y -= vector.y;

	return *this;
}

float vector2d::magnitude()
{
	return sqrt(this->x * this->x + this->y * this->y);
}

//Make Unit Vector
vector2d vector2d::normalize()
{
	float magnitude = this->magnitude();

	float nx = 0.0f;
	float ny = 0.0f;

	nx = this->x / magnitude;
	ny = this->y / magnitude;

	return vector2d(nx,ny);
}

float vector2d::operator*(const vector2d& rhs)
{
	return ( (this->x * rhs.x) + (this->y * rhs.y) );
}

float vector2d::crossProduct(const vector2d& vec)
{
	return (x * vec.y - y * vec.x);
}


