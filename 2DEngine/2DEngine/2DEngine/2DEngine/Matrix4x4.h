#pragma once
#ifndef __MATRIX4X4_H__
#define __MATRIX4X4_H__

#include <iostream>

using std::ostream;

class vector2d;

const float PI = 3.141316;

class Matrix4x4
{
public:
	float matrix[4][4];

	Matrix4x4();

	void setIdentity();
	void translate(float tx, float ty, float tz);
	void rotate(float angle);
	void scale(float sx, float sy);
	void transform(vector2d&);

	void Matrix4x4::mutliply(const float(&matrix)[4][4]);
	float* Matrix4x4::getPtr();

	friend ostream& operator<<(ostream& os, Matrix4x4&);
};

#endif
