#ifndef __SPRITE_BATCHER__
#define __SPRITE_BATCHER__

#include "Shader.h"
#include "VertexBufferArray.h"

class Quad;

class SpriteBatcher
{
	
	private:		
		size_t index;

		float *iBuffer;
		float *vBuffer;		
		
		size_t spriteCount;
		size_t indexOffset;
		size_t vertexOffset;

		Shader *shader;

		VertexBufferArray *vbaArray;
	public:			
		SpriteBatcher();

		void addSprite(const Quad &quad);
		void draw();
		void flushBatch();
		~SpriteBatcher();
};
#endif