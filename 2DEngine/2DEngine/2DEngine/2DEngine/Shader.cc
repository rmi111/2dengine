#include "shader.h"

Shader::Shader(const GLchar* vertexShaderPath, const GLchar* fragmentShaderPath)
{
	std::string vertexShaderCode;
	std::string fragmentShaderCode;

	std::ifstream vtShaderFile;
	std::ifstream fgShaderFile;

	vtShaderFile.open(vertexShaderPath);
	fgShaderFile.open(fragmentShaderPath);

	std::stringstream vtShaderStream;
	std::stringstream fgShaderStream;

	vtShaderStream << vtShaderFile.rdbuf();
	fgShaderStream << fgShaderFile.rdbuf();

	vertexShaderCode = vtShaderStream.str();
	fragmentShaderCode = fgShaderStream.str();

	const GLchar *vertexShaderRawCode = vertexShaderCode.c_str();
	const GLchar *fragmentShaderRawCode = fragmentShaderCode.c_str();

	GLuint vertexShader, fragmentShader;
	GLint success;
	GLchar infoLog[512];

	vertexShader = glCreateShader(GL_VERTEX_SHADER);

	glShaderSource(vertexShader, 1, &vertexShaderRawCode, NULL);

	glCompileShader(vertexShader);

	glGetShaderiv(vertexShader, GL_COMPILE_STATUS, &success);

	if (!success)
	{
		glGetShaderInfoLog(vertexShader, 512, NULL, infoLog);
		std::cout << "ERROR : VERTEX SHADER COMPILATION FAILED\n" << infoLog << std::endl;
	}
	
	fragmentShader = glCreateShader(GL_FRAGMENT_SHADER);

	glShaderSource(fragmentShader, 1, &fragmentShaderRawCode, NULL);

	glCompileShader(fragmentShader);

	glGetShaderiv(fragmentShader, GL_COMPILE_STATUS, &success);

	if (!success)
	{
		glGetShaderInfoLog(fragmentShader, 512, NULL, infoLog);
		std::cout << "ERROR : FRAGMENT SHADER COMPILATION FAILED\n" << infoLog << std::endl;
	}

	this->programId = glCreateProgram();

	glAttachShader(this->programId, vertexShader);
	glAttachShader(this->programId, fragmentShader);

	glLinkProgram(this->programId);

	glGetProgramiv(this->programId, GL_LINK_STATUS, &success);

	if (!success)
	{
		glGetProgramInfoLog(this->programId, 512, NULL, infoLog);
		std::cout << "ERROR : SHADER PROGRAM LINKING FAILED\n" << infoLog << std::endl;
	}

	glDeleteShader(vertexShader);
	glDeleteShader(fragmentShader);
}

GLuint Shader::getProgramId()
{
	return programId;
}

void Shader::useProgram()
{
	glUseProgram(this->programId);
}

void Shader::setUniformMatrix3(const GLchar *uniformName,float *value)
{
	GLint uniformLoc = glGetUniformLocation(this->programId,uniformName);
	glUniformMatrix3fv(uniformLoc,1,GL_FALSE,value);
}

void Shader::setUniformMatrix4(const GLchar *uniformName, float *value)
{
	GLint uniformLoc = glGetUniformLocation(this->programId, uniformName);
	glUniformMatrix4fv(uniformLoc, 1, GL_FALSE, value);
}